const { Router } = require("express");
const UserService = require("../services/userService");

const router = Router();

// TODO: Implement route controllers for user
router.get("/", (req, res, next) => {
	try {
		const data = UserService.getAll();
		res.data = data;
	} catch (err) {
		res.err = err;
	} finally {
		next();
	}
});

router.get("/:id", (req, res, next) => {
	try {
		const data = UserService.search((elem) => elem.id === req.params.id);
		res.data = data;
	} catch (err) {
		res.err = err;
	} finally {
		next();
	}
});

router.post("/", (req, res, next) => {
	try {
		if (!res.err) {
			const data = UserService.create(req.body);
			res.data = data;
		}
	} catch (err) {
		res.err = err;
	} finally {
		next();
	}
});

router.put("/:id", (req, res, next) => {
	try {
		if (!res.err) {
			const data = UserService.update(req.params.id, req.body);
			res.data = data;
		}
	} catch (err) {
		res.err = err;
	} finally {
		next();
	}
});

router.delete("/:id", (req, res, next) => {
	try {
		const data = UserService.delete(req.params.id);
		res.data = data;
	} catch (err) {
		res.err = err;
	} finally {
		next();
	}
});

module.exports = router;
