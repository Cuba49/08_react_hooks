import { useState } from "hooks/hooks";
import "./style.css";

const EditUserForm = ({ onClose, onSave, user }) => {
	const [currentUser, setCurrentUser] = useState(user);

	const handleChange = ({ target }) => {
		let { name, value } = target;
		if (name === "isAdmin") {
			value = !currentUser.isAdmin;
		}
		setCurrentUser({ ...currentUser, [name]: value });
	};
	const handleSubmit = (evt) => {
		evt.preventDefault();

		onSave(currentUser);
	};
	return (
		<>
			<div className="edit-message-modal modal-shown">
				<div className="edit-message-body">
					<div className="edit-inputs">
						<label>
							<span>Name:</span>
							<input
								value={currentUser.name}
								name="name"
								onChange={handleChange}
								type="text"
								placeholder="Name"
								required
							/>
						</label>
						<label>
							<span>Mail:</span>
							<input
								value={currentUser.mail}
								name="mail"
								onChange={handleChange}
								type="text"
								placeholder="Mail"
								required
								disabled={currentUser.id}
							/>
						</label>
						<label>
							<span>isAdmin:</span>
							<input
								name="isAdmin"
								onChange={handleChange}
								type="checkbox"
								placeholder="isAdmin"
								checked={currentUser.isAdmin}
							/>
						</label>
						{!currentUser.id && (
							<label>
								<span>Password:</span>
								<input
									value={currentUser.password}
									name="password"
									onChange={handleChange}
									type="text"
									placeholder="Password"
									required
								/>
							</label>
						)}
					</div>

					<div className="edit-message-bottom">
						<button
							className="edit-message-close"
							onClick={() => onClose()}
						>
							CLOSE
						</button>
						<button
							className="edit-message-button"
							onClick={handleSubmit}
						>
							SAVE
						</button>
					</div>
				</div>
			</div>
		</>
	);
};

export default EditUserForm;
