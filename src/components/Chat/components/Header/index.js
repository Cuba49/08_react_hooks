import "./style.css";
import { convertDateTime } from "helpers/dateHelper";
import { AppPath } from "common/enums/enums";

const Header = ({ name, countUsers, countMessages, lastMessageAt }) => {
	return (
		<div className="header">
			<div className="header-left">
				<p className="header-title">{name}</p>
				<a href={AppPath.USERS}>
					<p className="grey-text header-users-count">{countUsers}</p>
					<p className="grey-text">participants</p>
				</a>
				<p className="grey-text header-messages-count">{countMessages}</p>
				<p className="grey-text">messages</p>
			</div>
			<div className="header-right grey-text">
				<p>last message </p>
				<p className="header-last-message-date">
					{convertDateTime(lastMessageAt)}
				</p>
			</div>
		</div>
	);
};
export default Header;
