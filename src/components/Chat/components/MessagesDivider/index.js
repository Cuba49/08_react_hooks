import React from "react";
import "./style.css";
const MessagesDivider = ({ date }) => {
	return (
		<div className="messages-divider">
			<div className="messages-divider-date">{date}</div>
			<div className="messages-divider-line"></div>
		</div>
	);
};
export default MessagesDivider;
