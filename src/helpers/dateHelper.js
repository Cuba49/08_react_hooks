const convertToEngDate = (dateIso) => {
	const days = [
		"Sunday",
		"Monday",
		"Tuesday",
		"Wednesday",
		"Thursday",
		"Friday",
		"Saturday",
	];
	const months = [
		"January",
		"February",
		"March",
		"April",
		"May",
		"June",
		"July",
		"August",
		"September",
		"October",
		"November",
		"December",
	];
	const date = new Date(dateIso);
	const today = new Date();
	if (
		date.getFullYear() === today.getFullYear() &&
		date.getMonth() === today.getMonth() &&
		date.getDate() === today.getDate()
	) {
		return "Today";
	} else if (
		date.getTime() >
		new Date(
			"" +
				(today.getMonth() + 1) +
				"." +
				today.getDate() +
				"." +
				today.getFullYear()
		).getTime() -
			1000 * 60 * 60 * 24
	) {
		return "Yesterday";
	} else {
		const day = days[date.getDay()];
		const number = date.getDate();
		let month = months[date.getMonth()];
		return day + ", " + number + " " + month;
	}
};

const getFormat = (value) => (value < 10 ? `0${value}` : value);

const convertDate = (dateIso) => {
	const date = new Date(dateIso);

	const month = date.getMonth() + 1;
	const day = date.getDate();
	return `${getFormat(day)}.${getFormat(month)}.${date.getFullYear()}`;
};
const convertTime = (dateIso) => {
	const date = new Date(dateIso);
	const hours = date.getHours();
	const minutes = date.getMinutes();
	return `${getFormat(hours)}:${getFormat(minutes)}`;
};
const convertDateTime = (dateIso) => {
	return `${convertDate(dateIso)} ${convertTime(dateIso)}`;
};
// eslint-disable-next-line
export { convertToEngDate, convertDate, convertTime, convertDateTime };
