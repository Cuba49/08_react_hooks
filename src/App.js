import "./App.css";
import Chat from "./components/Chat";
import Users from "./components/Users";
import Login from "components/Login";
import { AppPath } from "./common/enums/enums";
import { Route, Switch } from "react-router-dom";

const App = () => (
	<>
		<Switch>
			<Route path={AppPath.ROOT} exact component={Chat} />
			<Route path={AppPath.USERS} exact component={Users} />
			<Route path={AppPath.LOGIN} exact component={Login} />
			<Route path={AppPath.ANY} exact component={Login} />
		</Switch>
	</>
);

export default App;
