import { combineReducers } from "redux";
import messages from "components/Chat/components/MessageList/reducer";
import editMessageForm from "components/Chat/components/EditMessageForm/reduser";
const rootReducer = combineReducers({
	messages,
	editMessageForm,
});

export default rootReducer;
