import { createReducer } from "@reduxjs/toolkit";
import { DataStatus, ActionStatus } from "common/enums/enums";
import { fetchUsers, addUser, updateUser, deleteUser, login } from "./actions";

const initialState = {
	users: [],
	status: DataStatus.IDLE,
	loginUser: null,
};

const reducer = createReducer(initialState, (builder) => {
	builder.addCase(fetchUsers.pending, (state) => {
		state.status = DataStatus.PENDING;
	});
	builder.addCase(fetchUsers.fulfilled, (state, { payload }) => {
		const { users } = payload;

		state.users = users;
		state.status = DataStatus.SUCCESS;
	});
	builder.addCase(addUser.fulfilled, (state, { payload }) => {
		const { user } = payload;

		state.users.unshift(user);
	});
	builder.addCase(updateUser.fulfilled, (state, { payload }) => {
		const { user } = payload;

		state.users = state.users.map((it) => {
			return it.id === user.id ? { ...it, ...user } : it;
		});
	});
	builder.addCase(login.fulfilled, (state, { payload }) => {
		console.log(payload);
		const { user } = payload;

		state.loginUser = user;
	});
	builder.addCase(deleteUser.fulfilled, (state, { payload }) => {
		const { user } = payload;

		state.users = state.users.filter((it) => it.id !== user.id);
	});
	builder.addMatcher(
		(action) => action.type.endsWith(ActionStatus.REJECTED),
		(state) => {
			state.status = DataStatus.ERROR;
		}
	);
});

export { reducer };
