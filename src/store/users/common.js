const ActionType = {
	FETCH_USERS: "users/fetch-users",
	ADD: "users/add",
	UPDATE: "users/update",
	DELETE: "users/delete",
	LOGIN: "users/login",
};

export { ActionType };
