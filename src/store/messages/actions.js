import { createAsyncThunk } from "@reduxjs/toolkit";
import { ActionType } from "./common";

const fetchMessages = createAsyncThunk(
	ActionType.FETCH_MESSAGES,
	async (_args, { extra }) => ({
		messages: await extra.messagesService.getAll(),
	})
);

const addMessage = createAsyncThunk(
	ActionType.ADD,
	async (payload, { extra }) => ({
		message: await extra.messagesService.create(payload),
	})
);

const updateMessage = createAsyncThunk(
	ActionType.UPDATE,
	async (payload, { extra }) => ({
		message: await extra.messagesService.update(payload),
	})
);

const deleteMessage = createAsyncThunk(
	ActionType.DELETE,
	async (message, { extra }) => {
		await extra.messagesService.delete(message.id);

		return {
			message,
		};
	}
);

export { fetchMessages, addMessage, updateMessage, deleteMessage };
