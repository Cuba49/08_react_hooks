import { configureStore } from "@reduxjs/toolkit";
import { messages, message, users } from "./root-reducer";
import {
	messages as messagesService,
	users as usersService,
} from "services/service";

const store = configureStore({
	reducer: {
		messages,
		message,
		users,
	},
	middleware: (getDefaultMiddleware) => {
		return getDefaultMiddleware({
			thunk: {
				extraArgument: {
					messagesService,
					usersService,
				},
			},
		});
	},
});
export { store };
